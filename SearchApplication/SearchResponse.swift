//
//  SearchResponse.swift
//  SearchApplication
//
//  Created by Zarina Syrymbet on 4/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

struct Image: Decodable {
    let tags: String
    let likes: Int
    let webformatURL: String
}

struct SearchResponse: Decodable {
    let hits: [Image]
}
