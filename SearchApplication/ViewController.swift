//
//  ViewController.swift
//  SearchApplication
//
//  Created by Zarina Syrymbet on 4/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var imageList = [Image]()
    var searchList = [Image]()
    private var manager = SearchManager()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        manager.loadData(onSuccess: { (searchResponse) in
            self.imageList = searchResponse.hits
            print("imageList.hits " , self.imageList.count)

        }) { (error) in
             print(error)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchList = searchText.isEmpty ? imageList : imageList.filter {
            (item: Image) -> Bool in
                return item.tags.range(of: searchText,options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailImageTableViewCell
                cell.configureCell(image: searchList[indexPath.row])
                return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ImageViewController") {
            if let viewController = segue.destination as? ImageViewController, let index =
                tableView.indexPathForSelectedRow?.row {
                viewController.cellValue = searchList[index]
            }
        }
    }
    
}





