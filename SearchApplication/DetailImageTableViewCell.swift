//
//  DetailImageTableViewCell.swift
//  SearchApplication
//
//  Created by Zarina Syrymbet on 4/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailImageTableViewCell: UITableViewCell {
    
    private let manager = SearchManager()
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var tags: UILabel!
    @IBOutlet weak var likes: UILabel!
    
    func configureCell(image: Image) {
        tags.text = "Tags: " + image.tags
        likes.text = "Likes: " + String(image.likes)
        manager.loadImage(url: image.webformatURL) { (data) in
            self.setImage(data: data)
        }
    }
    
    private func setImage(data: Data) {
        searchImage.image = UIImage(data: data)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0))
    }
    
}
