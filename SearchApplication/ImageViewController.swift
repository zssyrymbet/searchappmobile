//
//  ImageViewController.swift
//  SearchApplication
//
//  Created by Zarina Syrymbet on 4/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    var cellValue: Image?
    private let manager = SearchManager()
    @IBOutlet weak var image: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.loadImage(url: cellValue!.webformatURL) { (data) in
            self.setImage(data: data)
        }
    }
    
    private func setImage(data: Data) {
        image.image = UIImage(data: data)
    }
}
