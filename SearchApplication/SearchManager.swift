//
//  SearchManager.swift
//  SearchApplication
//
//  Created by Zarina Syrymbet on 4/23/20.
//  Copyright © 2020 Zarina Syrymbet. All rights reserved.
//

import UIKit
import Alamofire

class SearchManager {
    
    private let SEARCH_ENDPOINT = "https://pixabay.com/api/?key=3588873-8bb0e70fdfcef7f31eee25461"
    
    func loadData(onSuccess: @escaping (SearchResponse) -> Void, onFailure: @escaping (String) -> Void) {
        AF.request(SEARCH_ENDPOINT).responseDecodable { (response: DataResponse<SearchResponse, AFError>) in
            switch response.result {
                case .success(let searchResponse):
                    onSuccess(searchResponse)
                case .failure(let error):
                    onFailure(error.localizedDescription)
            }
        }
    }
    
    func loadImage(url: String, onSuccess: @escaping (Data) -> Void) {
        AF.request(url).responseImage { (image) in
            if image.data != nil {
                onSuccess(image.data!)
            }
        }
    }

}
